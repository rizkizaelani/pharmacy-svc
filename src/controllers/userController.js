const boom = require('boom');

// Get Data Models
const User = require('../models/Users');

// Get All Users
exports.getUsers = async (req, res) => {
    try {
        const users = await User.find()
        return users
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Get User by ID
exports.getUserById = async (req, res) => {
    try {
        const id = req.params.id
        const user = await User.findById(id)
        return user
    } catch (err) {
        throw boom.boomify(err)
    }
}

exports.addUser = async (req, res) => {
    try {
        const user = new User(req.body)
        return user.save()
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Update an Existing User
exports.updateUser = async (req, res) => {
    try {
        const id = req.params.id
        const user = req.body
        const { ...updateData } = user
        const update = await User.findByIdAndUpdate(id, updateData, { new: true })
        return update
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Delete a User
exports.deleteUser = async (req, res) => {
    try {
        const id = req.params.id
        const user = await User.findByIdAndRemove(id)
        return user
    } catch (err) {
        throw boom.boomify(err)
    }
}